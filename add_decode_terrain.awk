# Decode terrain data found in SCP island files.
# This script is used internally by 'add'.
#
# Dependencies: GNU awk 5.x

# This file is part of add.
# SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
# SPDX-License-Identifier: GPL-3.0-or-later

# TERRAIN DATA:
# - found in GFXID blocks in SCP files
# - starts just like HEIGHT: 2b X, 2b Y, 1b W, 1b H
# - then 2 bytes per field:
#   - SECOND byte: base texture id
#   - FIRST byte: variation -> decoded with decode_terrain(...)
#       - each texture file (256*256px) contains 2x2 smaller textures (quarters)
#       - each quarter is split into 3x3 fields (43x43px each)
#
#       HOW TO DECODE:
#         EXPLANATION:
#         - variations are certain values added together (+)
#         - they can be determined by using base 4 and base 8 as representation
#
#         - first: determine rotation/orientation/angle
#               x%4 = rotation (4 options) → lowest digit in base 4
#
#               base 4:  base 10:
#               A        A
#               00       00            clockwise: | 1:  0  2: 90 |
#               01       01                       | 4:270  3:180 |
#               02       02
#               03       03
#
#         - then: determine column (3 options), row (3 options), and quarter (2x2 options)
#               1. Drop lowest digit in base 4:
#                   x -= (x%4)
#                   x /= 4
#
#               2. Data:
#
#                   base 8:         | base 10:
#                   Col   Row   Tex | Col   Row   Tex
#                   00    00    00  | 00    00    00
#                         01        |       01
#                         02        |       02
#                               03  |             03
#                   10              | 08
#                   20              | 16
#                               30  |             24
#                               33  |             27
#
#                   Quarters:
#
#                       top left -> bottom left -> bottom right -> top right
#
#                         T           T
#                       T 1:(0,   0)  4:(128, 128)
#                       T 2:(0, 128)  3:(128,   0)
#
#                   Rows and Columns:
#
#                         C         C          C
#                       R 1:(0,  0) 2:(42,  0) 3:(85,  0)
#                       R 2:(0, 42)   (42, 42)   (85, 42)
#                       R 3:(0, 85)   (42, 85)   (85, 85)
#
#                   Example:
#
#                         CCC
#                       R ---
#                       R -x- => C=01 + R=10 = 11 (base 8) = 9 (base 10)
#                       R ---
#
#               3. Drop lowest digit in base 8:
#                   x -= (x%8) # remove last digit
#                   x /= 8 # rshift
#
function decode_terrain(encoded, results) {
    # encoded: encoded terrain value, e.g. 37
    # results: array where results will be stored
    #          keys: Q, R, C, A

    __x=encoded

    # Q: texture quarter, C: column, R: row, A: angle (orientation)
    __Q=0; __C=0; __R=0; __A=0;
    __Qlo=0; __Qup=0 # temporary

    # orientation: lowest digit in base 4
         if (__x%4 == 0) __A=0
    else if (__x%4 == 1) __A=1
    else if (__x%4 == 2) __A=2
    else if (__x%4 == 3) __A=3
    __x -= (__x%4)  # remove last digit
    __x /= 4      # rshift

    # row and quarter: next lowest digit in base 8
         if (__x%8 == 0)   {             }
    else if (__x%8 == 1)   { __R=1;        }
    else if (__x%8 == 2)   { __R=2;        }
    else if (__x%8 == 3)   {      __Qlo=1; }
    else if (__x%8 == 1+3) { __R=1; __Qlo=1; }
    else if (__x%8 == 2+3) { __R=2; __Qlo=1; }
    __x -= (__x%8)  # remove last digit
    __x /= 8      # rshift

    # column and quarter: last remaining digit in base 8
         if (__x%8 == 0)   {             }
    else if (__x%8 == 1)   { __C=1;        }
    else if (__x%8 == 2)   { __C=2;        }
    else if (__x%8 == 3)   {      __Qup=1; }
    else if (__x%8 == 1+3) { __C=1; __Qup=1; }
    else if (__x%8 == 2+3) { __C=2; __Qup=1; }
    __x -= (__x%8)  # remove last digit
    __x /= 8      # rshift

    # determine quarter
         if (__Qlo == 1 && __Qup == 1) { __Q=2; }
    else if (__Qlo == 1            )   { __Q=3; }
    else if (            __Qup == 1)   { __Q=1; }

    results["Q"] = __Q
    results["R"] = __R
    results["C"] = __C
    results["A"] = __A

    if (__x != 0) {
        return -1
    } else {
        return 0
    }
}
