#!/bin/bash

# This file is part of add.
# SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
# SPDX-License-Identifier: GPL-3.0-or-later

# Convert OBJ 3D object files to GLTF
#
# Dependencies: Blender 4.0

if [[ ! -f "$1" ]]; then
    echo "usage: $0 OBJ"
    echo "will be exported to ./export/OBJ.gltf"
    exit 1
fi

mkdir -p export
file="${1//\"/\\\"}"
printf "exporting to ./export/%s" "$file"

blender -b --python-console <<EOF
import bpy
bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete()  # clear the scene
bpy.ops.import_scene.obj(filepath="$file")
bpy.ops.export_scene.gltf(export_format='GLTF_SEPARATE', filepath="./export/$file")
EOF
