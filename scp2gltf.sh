#!/bin/bash

# This file is part of add.
# SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
# SPDX-License-Identifier: GPL-3.0-or-later

# Convert SCP island files to GLTF 3D objects
#
# usage: scp2gltf.sh island.scp
# output: island.gltf
#
# Dependencies: working add plus Blender 4.0

if [[ ! -f "$1" ]]; then
    echo error: input not found
    exit 1
fi

base="${1%.scp}"
xml="$base.xml"

./add -x "$1" -S ./spec/s_insel.xml | xml_pp > "$xml"
./add --d-scp-mesh "$xml"
./export_blender.sh "$base.obj"

# ./add --d-scp-terrain "$xml"
# ./add --d-scp-decode-terrain "$xml"
