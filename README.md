<!--
This file is part of add.
SPDX-FileCopyrightText: 2020-2024  Emily Margit Mueller
SPDX-License-Identifier: GPL-3.0-or-later
-->

# Anno 1503 Data Digger

`add` is a tool for analyzing, converting, and manipulating Anno 1503 game data
files. It is entirely written in Bash, and uses `dd` to read binary files.

Most if not all functionality is ported into the successor projects
[amsel](https://gitlab.com/millimarg/amsel) and
[add-py (outdated)](https://gitlab.com/millimarg/add-py).


## Project status

**FINISHED BUT NO LONGER USABLE.**

The code in this repository is kept for posterity only. It should not be used
as an example for any new projects. It is outdated, limited, and depends on
outdated versions of [ax](https://gitlab.com/millimarg/ax).

Use [A₂E](https://gitlab.com/millimarg/ae) or
[amsel](https://gitlab.com/millimarg/amsel) instead.


## License

Copyright (C) 2020-2024  Emily Margit Mueller (millimarg)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <a href="https://www.gnu.org/licenses/">www.gnu.org/licenses</a>.
