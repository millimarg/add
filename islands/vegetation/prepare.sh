#!/bin/bash

if [[ ! -f "$1" ]]; then
    echo error: input not found
    exit 1
fi

xml="${1%.scp}.xml"
../add -x "$1" -S ../spec/s_insel.xml | xml_pp > "$xml"
../add --d-scp-vegetation "$xml"
../add --d-scp-terrain "$xml"
# ../add --d-scp-mesh "$xml"
# ../add --d-scp-decode-terrain "$xml"
